# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = Shanty

CONFIG += sailfishapp

SOURCES += src/Shanty.cpp \
    src/filecounter.cpp

HEADERS += \
    src/filecounter.h

OTHER_FILES += qml/Shanty.qml \
    qml/cover/CoverPage.qml \
    rpm/Shanty.spec \
    rpm/Shanty.yaml \
    Shanty.desktop \
    LICENSE \
    qml/pages/AlbumPage.qml \
    qml/pages/TrackPage.qml

HEADERS +=

