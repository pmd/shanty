// filecounter.cpp
// Copyright (C) 2016 Patrick McDonough <dev@awk.is>

#include "filecounter.h"

FileCounter::FileCounter(QObject *parent) :
    QObject(parent)
{
}

int FileCounter::getTrackCount(QString path)
{
    QDir directory(path);
    directory.setNameFilters(QStringList() << "*.opus" << "*.ogg" << "*.flac" << "*.aac");
    return directory.count();
}

int FileCounter::getDirCount(QString path)
{
    QDir directory(path);
    directory.setFilter(QDir::AllDirs);
    return directory.count() - 2;
}
