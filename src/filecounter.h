// filecounter.h
// Copyright (C) 2016 Patrick McDonough <dev@awk.is>

#ifndef FileCounter_H
#define FileCounter_H

#include <QObject>
#include <QDir>

class FileCounter : public QObject
{
    Q_OBJECT
public:
    explicit FileCounter(QObject *parent = 0);
    Q_INVOKABLE int getTrackCount(QString path);
    Q_INVOKABLE int getDirCount(QString path);

signals:

public slots:

};

#endif // FileCounter_H
