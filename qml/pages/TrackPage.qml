// TrackPage.qml
// Copyright (C) 2016 Patrick McDonough <dev@awk.is>

import QtQuick 2.0
import Sailfish.Silica 1.0
import Qt.labs.folderlistmodel 1.0
import QtMultimedia 5.0

Page {
    id: secondPage
    // Pass the album path (for the page title)
    // Pass the coverPage so we can continue talking to it.
    // Pass the audio player itself, so we can push files onto it.
    property string path: ""
    property CoverBackground coverPage
    property Audio player

    // Current source is bound to the current player source.
    // Used for setting highlight status when returning to a previously destroyed AlbumPage.
    // i.e. Play a track in an album A, open the page for Album B, don't select anything,
    // and return to original album.
    property string currentSource

    // Repeating is handled by checking a string (None/Track/Album)
    // Shuffle is just a boolean.
    property string repeatMode: "None"
    property bool shuffle: false

    // priorTrackItem is the last track delegate played
    // currentTrackItem is the current track delegate.
    // currentTrackIndex is the index of the currentTrackItem (starting from zero)
    // count is track count and doesn't change.
    property var priorTrackItem
    property alias currentTrackItem: trackListView.currentItem
    property alias currentTrackIndex: trackListView.currentIndex
    property alias currentTrackListCount: trackListView.count

    // Populate a model with music files.
    FolderListModel
    {
             id: trackListModel
             showDirs: false
             nameFilters: [ "*.opus", "*.ogg", "*.flac", "*.aac", "*.mp3"]
             folder: path
             sortField: FolderListModel.Name
    }

    // Create a binding on currentSource - when currentSource changes,
    // AlbumPage.player.source is updated to reflect the new track.
    Binding on currentSource
    {
        when: player.playbackState === Audio.PlayingState
        value: player.source
    }

    Item
    {
        id: playerInterface

        // 99 is arbitrary - we just need an initial value.
        // Once we start playing, it'll be track the audio player's state.
        property int playerStatus: 99
        Binding on playerStatus
        {
            when: player.availability === Audio.Available
            value: player.status
        }

        // When that state changes, we check to see if we need to load a new track.
        onPlayerStatusChanged: if (playerStatus === Audio.EndOfMedia) { playNext() }

        // Paused? Unpause. Playing? Pause.
        function togglePause()
        {
            if (player.playbackState === Audio.PausedState)
            {
                currentTrackItem.labelColour = Theme.highlightColor
                player.play()
            }
            else if (player.playbackState === Audio.PlayingState)
            {
                currentTrackItem.labelColour = Theme.secondaryHighlightColor
                player.pause()
            }
        }

        // An immediate play. Called when a user selects a track.
        // Functions are responsible for tidying up the highlights of items they touch.
        function playSpecific(index)
        {
            priorTrackItem = currentTrackItem
            priorTrackItem.labelColour = Theme.primaryColor
            currentTrackIndex = index
            currentTrackItem.labelColour = Theme.highlightColor

            player.source = currentTrackItem.trackPath
            player.play()

            // Refresh cover art to reflect track change.
            coverPage.updateArt(path + "folder.jpg")
        }

        // This is a real mess of checking.
        function playNext()
        {
            // No prior track? Must be a new album. Play track 0.
            // playNext() can only call in this way from the cover action.
            if (priorTrackItem === undefined)
                playSpecific(0)
            else
            {
                // We must have a previous track.
                // This is how its done throughout.
                priorTrackItem = currentTrackItem
                priorTrackItem.labelColour = Theme.primaryColor
                // Track repeat is on, just stop and undo that change of highlight.
                // play() is called at the end of playNext().
                if (repeatMode == "Track")
                {
                    currentTrackItem.labelColour = Theme.highlightColor
                    player.stop()
                }
                // Shuffle is on? Then we don't care what track is next. Pick one at random,
                // and update player.source.
                else if (shuffle === true)
                {
                    currentTrackIndex = Math.floor(Math.random() * currentTrackListCount)
                    currentTrackItem.labelColour = Theme.highlightColor
                    player.source = currentTrackItem.trackPath
                }
                // Shuffle is off. Are we at the end of the album?
                else if (currentTrackIndex === (currentTrackListCount - 1))
                {
                    // If yes, reset the current index to 0.
                    currentTrackIndex = 0
                    // Start playing again if repeat album is on, or stop if not.
                    if (repeatMode === "Album")
                    {
                        currentTrackItem.labelColour = Theme.highlightColor
                        player.source = currentTrackItem.trackPath
                    }
                    else if (repeatMode === "None")
                    {
                        player.source = ""
                    }
                }

                // If it isn't the last trackl, we increment the index, and update the current source.
                else
                {
                    currentTrackIndex += 1
                    currentTrackItem.labelColour = Theme.highlightColor
                    player.source = currentTrackItem.trackPath
                }

                // Reposition the view, and play.
                trackListView.positionViewAtIndex(currentTrackIndex, ListView.Center)
                player.play()

                // Refresh cover art to reflect track change.
                coverPage.updateArt(path + "folder.jpg")
            }
        }

        // When the interface is ready, tell the cover so it can start sending coverAction events.
        Component.onCompleted: coverPage.interfaceCreated(playerInterface)
    }

    SilicaListView
    {
        VerticalScrollDecorator {}

        id: trackListView

        model: trackListModel
        width: Screen.width
        height: Screen.height

        // Hacky. But simple.
        header: PageHeader { title: (path.replace("file://" + StandardPaths.music + "/", "")).slice(0, -1) }

        highlightFollowsCurrentItem: true
        quickScrollEnabled: true

        // When this is true, +1 to currentIndex (when on the last item) will yeild the first item.
        keyNavigationWraps: false

        PullDownMenu
        {
            MenuItem
            {
                id: menuRepeat
                text: "Repeat Mode: None"
                onClicked:
                {
                    switch (repeatMode)
                    {
                    case "None":
                        repeatMode = "Track"
                        menuRepeat.text = "Repeat Mode: Track"
                        break
                    case "Track":
                        repeatMode = "Album"
                        menuRepeat.text = "Repeat Mode: Album"
                        trackListView.keyNavigationWraps = true
                        break
                    case "Album":
                        repeatMode = "None"
                        menuRepeat.text = "Repeat Mode: None"
                        trackListView.keyNavigationWraps = false
                        break
                    }
                }
            }

            MenuItem
            {
                id: menuShuffle
                text: "Shuffle: Off"
                onClicked:
                {
                    switch (shuffle)
                    {
                    case false:
                        shuffle = true
                        text = "Shuffle: On"
                        break
                    case true:
                        shuffle = false
                        text = "Shuffle: Off"
                        break
                    }
                }
            }
        }

        PushUpMenu
        {
            Slider
            {
                id: progressBar
                height: Theme.itemSizeSmall
                width: (parent.width - Theme.paddingSmall)
                Binding on value { value: player.position }
                Binding on maximumValue { value: player.duration }
                onPressedChanged: player.seek(value)
            }


        }

        delegate: BackgroundItem
        {

            id: trackDelegate
            height: Theme.itemSizeSmall

            property string trackPath: (path + fileName)
            property alias labelColour: localTitle.color

            Label {
                id: localTitle
                text: fileName
                fontSizeMode: Theme.fontSizeExtraSmall
                truncationMode: TruncationMode.Fade
                // Text should be highlighted when playing and when its parent is pressed down.
                color: (trackDelegate.highlighted || trackPath == currentSource ) ? Theme.highlightColor : Theme.primaryColor
                anchors
                {
                    margins: Theme.paddingLarge
                    left: parent.left
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                }
            }

            onClicked:
            {
                // If we've returned to an album page, and we're playing, we
                // should make sure to re-apply highlight.
                if (trackPath == currentSource) { currentTrackIndex = index }

                if (trackPath == currentSource)
                    playerInterface.togglePause()
                else
                    playerInterface.playSpecific(index)
            }
        }
    }
}
