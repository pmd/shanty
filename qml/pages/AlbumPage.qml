// AlbumPage.qml
// Copyright (C) 2016 Patrick McDonough <dev@oawk.is>

import QtQuick 2.0
import Sailfish.Silica 1.0
import Qt.labs.folderlistmodel 1.0
import local.filecounter 0.1
import QtMultimedia 5.0

Page {
    id: firstPage
    // Keep track of the cover page, we're going to be talking to it.
    property var coverPage: app.coverPage
    property var trackIndicator: app.coverPage.trackIndicator
    property int expectedDelegates: musicLibrary.count
    property string currentAlbum: ""

    FolderListModel
    {
        id: musicLibrary
        showDirs: true
        folder: StandardPaths.music
    }

    FileCounter { id: fileCounter }

    SilicaListView
    {
        id: listView
        model: musicLibrary

        VerticalScrollDecorator {}

        width: Screen.width
        height: Screen.height

        quickScrollEnabled: true

        header: PageHeader {
            title:qsTr("Shanty Music Player")
        }

        Column {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            visible: expectedDelegates === 0 ? true : false

            Label
            {
                text: "No music"
                color: Theme.highlightColor
                anchors.horizontalCenter: parent.horizontalCenter
                fontSizeMode: Theme.fontSizeLarge
            }

            Label
            {
                text: "Add your files to ~/Music"
                color: Theme.secondaryHighlightColor
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }

        delegate: BackgroundItem
        {
            id: albumListDelegate

            height: Theme.itemSizeLarge
            width: Screen.width

            Label
            {
                id: firstLetter
                text: (fileName.charAt(0) + fileName.charAt(1))
                font.pixelSize: (Theme.fontSizeHuge + 2)
                height: Theme.itemSizeLarge
                width: Theme.itemSizeLarge
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                color: Theme.highlightColor
            }

            Column
            {
                anchors.left: firstLetter.right
                anchors.leftMargin: Theme.paddingMedium
                anchors.verticalCenter: parent.verticalCenter
                width: parent.width - firstLetter.width

                Label
                {
                    width: parent.width
                    text: fileName
                    verticalAlignment: TextEdit.AlignVCenter
                    truncationMode: TruncationMode.Fade

                    color: albumListDelegate.highlighted ? Theme.highlightColor : Theme.primaryColor
                }
                Label
                {
                    width: parent.width
                    text: (fileCounter.getTrackCount("Music/" + fileName)) + " tracks"
                    verticalAlignment: TextEdit.AlignVCenter
                    truncationMode: TruncationMode.Fade
                    font.pixelSize: Theme.fontSizeSmall
                    color: albumListDelegate.highlighted ? Theme.highlightColor : Theme.secondaryColor
                }
            }

            onClicked:
            {
                // Pass the album path and player object to the newly-created track page.
                pageStack.pushAttached (Qt.createComponent("TrackPage.qml"), {
                                    path: (musicLibrary.folder + '/' + fileName + '/'),
                                    coverPage: coverPage,
                                    player: audioPlayer })
                // Slide forward automatically.
                pageStack.navigateForward(PageStackAction.Animated)

            }
        }
    }

    Audio
    {
        id: audioPlayer
        source: ""

        onPlaybackStateChanged:
        {
            if (this.playbackState === Audio.PlayingState)
                coverPage.audioPlaying()
            if (this.playbackState === Audio.PausedState)
                coverPage.audioPaused()
            if (this.playbackState === Audio.StoppedState)
                coverPage.audioStopped()
        }
    }
}
