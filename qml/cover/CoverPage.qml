// CoverPage.qml
// Copyright (C) 2016 Patrick McDonough <dev@awk.is>

import QtQuick 2.0
import Sailfish.Silica 1.0
import local.filecounter 0.1
import QtGraphicalEffects 1.0

CoverBackground {
    id: coverPage

    property Item player

    signal interfaceCreated(Item playerInterface)
    onInterfaceCreated: player = playerInterface

    signal audioPlaying()
    signal audioPaused()
    signal audioStopped()
    signal updateArt(string artPath)

    onAudioPlaying:
    {
        coverDefault.enabled = false
        playPauseAction.iconSource = "image://theme/icon-cover-pause"
    }

    onAudioPaused:  playPauseAction.iconSource = "image://theme/icon-cover-play"

    onAudioStopped: coverDefault.enabled = true

    onUpdateArt: albumArtImage.source = artPath

    FileCounter { id: fileCounter }

    // Inactive cover
    Column
    {
        id: coverDefault
        enabled: true
        anchors.centerIn: parent

        Image
        {
            visible: parent.enabled
            id: image
            anchors.horizontalCenter: parent.horizontalCenter
            source: "image://theme/icon-l-music"
        }

        Label
        {
            visible: parent.enabled
            text: (fileCounter.getDirCount(StandardPaths.music) + " albums")
            anchors.horizontalCenter: parent.horizontalCenter
            color: Theme.primaryColor
            font.pixelSize: Theme.fontSizeSmall
        }
    }

    // Active cover
    Image {
       id: albumArtImage
       enabled: coverDefault.enabled ? false : true
       smooth: true
       width: (Theme.coverSizeLarge).height
       height: (Theme.coverSizeLarge).height
       anchors.centerIn: parent

       Binding on source
       {
           when: enabled = true
           value: player.artPath
       }
    }

    OpacityRampEffect
    {
        id: ramp
        sourceItem: albumArtImage
        direction: OpacityRamp.TopToBottom
    }

    CoverActionList
    {
        id: coverActions
        enabled: coverDefault.enabled ? false : true

        CoverAction {
            id: playPauseAction
            iconSource: "image://theme/icon-cover-play"
            onTriggered: player.togglePause()
        }

        CoverAction {
            iconSource: "image://theme/icon-cover-next-song"
            onTriggered: player.playNext()
        }
    }
    Component.onCompleted: app.coverCreated(coverPage)
}
