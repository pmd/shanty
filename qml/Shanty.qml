// Shanty.qml
// Copyright (C) 2016 Patrick McDonough <dev@awk.is>

import QtQuick 2.0
import Sailfish.Silica 1.0
import QtMultimedia 5.0
import "pages"
import "cover"

ApplicationWindow
{
    id: app

    property int position

    signal coverCreated(var cover)
    property var coverPage
    onCoverCreated: coverPage = cover

    cover: Component { CoverPage {} }
    initialPage: Component { AlbumPage {} }
}
